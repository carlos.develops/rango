from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from rangoapp import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^rango/', include('rangoapp.urls')), # rangoapp urls handle
    url(r'^admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) # 1to serve static content from MEDIA_URL.
