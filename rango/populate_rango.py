import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'rango.settings')

import django
django.setup()
from rangoapp.models import Category, Page

def populate():
    # First, we will create lists of dictionaries containing the pages
    # we want to add into each category.
    # Then we will create a dictionary of dictionaries for our categories.
    # This might seem a little bit confusing, but it allows us to iterate
    # hrough each data structure, and add the data to our models.

    python_pages = [
        {"title": "Official Python Tutorial", "url":"http://docs.python.org/2/tutorial/", "views": 17},
        {"title":"How to Think like a Computer Scientist", "url":"http://www.greenteapress.com/thinkpython/", "views": 2},
        {"title":"Learn Python in 10 Minutes", "url":"http://www.korokithakis.net/tutorials/python/", "views": 7}
    ]

    django_pages = [
        {"title":"Official Django Tutorial", "url":"https://docs.djangoproject.com/en/1.9/intro/tutorial01/", "views": 14},
        {"title":"Django Rocks", "url":"http://www.djangorocks.com/", "views": 4},
        {"title":"How to Tango with Django", "url":"http://www.tangowithdjango.com/", "views": 8}
    ]

    other_pages = [
        {"title":"Bottle", "url":"http://bottlepy.org/docs/dev/", "views":12},
        {"title":"Flask", "url":"http://flask.pocoo.org", "views":8}
    ]

    cats = {
        "Python": {"pages": python_pages, "views_cat": 128, "likes_cat": 64},
        "Django": {"pages": django_pages, "views_cat": 64, "likes_cat": 32},
        "Other Frameworks": {"pages": other_pages, "views_cat": 32, "likes_cat": 16}
    }

    # The code below goes through the cats dictionary, then adds each category,
    # and then adds all the associated pages for that category.
    # if you are using Python 2.x then use cats.iteritems() see
    # http://docs.quantifiedcode.com/python-anti-patterns/readability/
    # for more information about how to iterate over a dictionary properly.

    for cat, cat_data in cats.items():
        c = add_cat(cat, cat_data["views_cat"], cat_data["likes_cat"])
        for p in cat_data["pages"]:
            add_page(c, p["title"], p["url"], p['views'])

    # Print out the categories we have added.
    for c in Category.objects.all():
        for p in Page.objects.filter(category=c):
            print("- {0} - {1}".format(str(c), str(p)))

def add_page(cat, title, url, views=0):
    p = Page.objects.get_or_create(category=cat, title=title)[0] # [0] at the end of our call to the get_or_create() returns the object reference only.
    p.url = url
    p.views = views
    p.save()
    return p

def add_cat(name, views_cat=0, likes_cat=0):
    c = Category.objects.get_or_create(name=name)[0]
    c.views = views_cat
    c.likes = likes_cat
    c.save()
    return c

# Start execution here!
print(__name__ == '__main__')
if __name__ == '__main__':
    print("Starting Rango Population script...")
    populate()
